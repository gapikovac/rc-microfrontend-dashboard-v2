import React from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import styled from 'styled-components';
import Loading from './Loading';
import PhotoList from './PhotoList';

const MainColumn = styled.div`
  max-width: 1150px;
  margin: 0 auto;
`;

const defaultHistory = createBrowserHistory();

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Router history={this.props.history || defaultHistory}>
        <MainColumn>
          <h1>Dashboard v2</h1>
        </MainColumn>
      </Router>
    );
  }
}

export default App;
